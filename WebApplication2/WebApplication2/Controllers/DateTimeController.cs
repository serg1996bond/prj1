﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Text;

namespace WebApplication2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DateTimeController : ControllerBase
    {
        private readonly ILogger<DateTimeController> _logger;

        public DateTimeController(ILogger<DateTimeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public HttpResponseMessage GetFormated(String format)
        {
            var response = new HttpResponseMessage();
            if (format == null) {
                response.Content = new StringContent(DateTime.Now.ToString());
                response.StatusCode = HttpStatusCode.OK;
            }else if (Regex.IsMatch(format, @"[dfgmor]", RegexOptions.IgnoreCase)) {
                response.Content = new StringContent(DateTime.Now.ToString(format), Encoding.Unicode);
                response.StatusCode = HttpStatusCode.OK;
            } else {
                response.StatusCode = HttpStatusCode.BadRequest;
            }
            _logger.LogInformation("response {response} are sanded", response);
            return response;
        }
    }
}
